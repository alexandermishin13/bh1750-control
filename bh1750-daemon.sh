#!/bin/sh
#
# $FreeBSD: 2020-09-10 13:20:00Z mishin $
#
# PROVIDE: bh1750_daemon
# REQUIRE: DAEMON
# KEYWORD: nojail shutdown
#
# Add the following lines to /etc/rc.conf to enable bh1750_daemon:
#
# bh1750_daemon_enable (bool):	Set to "NO"  by default.
#				Set to "YES" to enable bh1750_daemon.
# bh1750_daemon_profiles (str):	Set to "" by default.
#				Define your profiles here.
# bh1750_daemon_devnum (str):	Set to "0" by default.
#				Define a cdev number
# bh1750_daemon_dbfile (str):	Set to "/var/db/bh1750/actions.sqlite" by default.
#				Set sqlite3 db file here
# bh1750_daemon_flags (str):	Set to "-b" by default.
#				Extra flags passed to start command.
#
# For a profile based configuration use variables like this:
#
# bh1750_daemon_profiles="XXX"
# bh1750_daemon_XXX_devnum="0"
# bh1750_daemon_XXX_dbfile="/var/db/bh1750/actions.sqlite"
# bh1750_daemon_XXX_flags="-b"

. /etc/rc.subr

name=bh1750_daemon
rcvar=bh1750_daemon_enable

load_rc_config $name

: ${bh1750_daemon_enable:="NO"}
: ${bh1750_daemon_flags:="-b"}
: ${bh1750_daemon_devnum:="0"}
: ${bh1750_daemon_dbfile:="/var/db/bh1750/actions.sqlite"}
: ${bh1750_daemon_user:="root"}
: ${bh1750_daemon_group:="wheel"}

pidfile=${bh1750_daemon_pidfile}
bh1750_daemon_bin="/usr/local/sbin/bh1750-daemon"

is_profile_exists() {
	local profile

	for profile in $bh1750_daemon_profiles; do
		[ "$profile" = "$1" ] && return 0;
	done

	return 1
}

if [ -n "${bh1750_daemon_profiles}" ]; then
	if [ -n "$2" ]; then
		profile="$2"
		if ! is_profile_exists $profile; then
			echo "$0: no such profile defined in bh1750_daemon_profiles."
			exit 1
		fi
		eval bh1750_daemon_devnum=\${bh1750_daemon_${profile}_devnum:-"${bh1750_daemon_devnum}"}
		eval bh1750_daemon_dbfile=\${bh1750_daemon_${profile}_dbfile:-"${bh1750_daemon_dbfile}"}
		bh1750_daemon_devfile="/dev/bh1750/${bh1750_daemon_devnum}"
		pidfile="/var/run/bh1750-daemon/bh1750-${bh1750_daemon_devnum}.pid"
	elif [ -n "$1" ]; then
		for profile in ${bh1750_daemon_profiles}; do
			echo "===> bh1750-daemon profile: ${profile}"
			/usr/local/etc/rc.d/bh1750-daemon $1 ${profile}
			retcode="$?"
			if [ "0${retcode}" -ne 0 ]; then
				failed="${profile} (${retcode}) ${failed:-}"
			else
				success="${profile} ${success:-}"
			fi
		done
		exit 0
	fi
fi

command=${bh1750_daemon_bin}
command_args="-s ${bh1750_daemon_devfile} -f ${bh1750_daemon_dbfile} -p ${pidfile} ${bh1750_daemon_flags}"

start_precmd="${name}_precmd"

bh1750_daemon_precmd()
{
	install -d -o ${bh1750_daemon_user} -g ${bh1750_daemon_group} /var/run/bh1750-daemon
}

run_rc_command "$@"
